<?php
$input_number = file_get_contents('input.txt');
$arr_number = explode("\n", $input_number);

$duplicate_num = [];
for ($i = 0; $i < count($arr_number); $i++) {
    $num = $arr_number[$i];
    $count = 0;
    for ($j = 0; $j < count($arr_number); $j++) {
        if ($arr_number[$j] == $num) $count++;

        if ($count == 3) {
            if (!in_array($num, $duplicate_num)) {

                array_push($duplicate_num, $num);
            }
                break;
        };
    }
}
if (count($duplicate_num) > 0) {
    echo 'Duplicate number: ' . implode(', ', $duplicate_num)."\n";    
} else {
    echo 'No duplicate number!';
}
?>
