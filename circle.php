<?php
// echo "HuuKha";
class Circle {
    public function get_perimeter($radius) {
        echo 'perimeter: '. round(2*pi()*$radius, 2);
    }

    public function get_acreage($radius) {
        echo 'acreage: '. round($radius*$radius*pi(), 2);
    }
}

if (isset($argv[1])) {
    $radius = $argv[1];
} else {
    $radius = null;
}

function check_radius($radius) {
    if (isset($radius)) {
        if (preg_match('/^[0-9.-]+$/', $radius)) {
            $radius = floatval($radius);
            if ($radius > 0) {
                return true;
            } else {
                echo "Please enter number greater than 0";
                return false;
            }
        } else {
            echo "Please enter valid number";
            return false;
        }
    } else {
        echo "Please enter number";
        return false;
    }

    
}

$check_radius = check_radius($radius);

if ($check_radius) {
    $circle = new Circle();
    $circle->get_perimeter($radius);
    echo "\n";
    $circle->get_acreage($radius);
}
?>